﻿using SitefinityWebApp.Attributes;
using SitefinityWebApp.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SitefinityWebApp.Mvc.Models
{
    public class TorrentWidgetModel
    {
        [Required]
        [MinLength(3)]
        public string Title { get; set; }

        public string ImageId { get; set; }

        public string FileId { get; set; }

        [Required]
        [MinLength(10)]
        public string Description { get; set; }

        [Required]
        public string AdditionalInfo { get; set; }

        [Required]
        public List<string> Genres { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime DateCreated { get; set; }

        [Required]
        public CultureNames Culture { get; set; }

        [ImageType(ErrorMessage = "The file must be an image")]
        public HttpPostedFileBase ImageFile { get; set; }

        [TorrentType(ErrorMessage = "The file must be a torrent")]
        public HttpPostedFileBase TorrentFile { get; set; }

    }
}