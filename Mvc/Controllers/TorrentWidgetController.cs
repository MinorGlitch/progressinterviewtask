﻿using SitefinityWebApp.Mvc.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using SitefinityWebApp.Services.Torrent;
using AutoMapper;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Lifecycle;
using SitefinityWebApp.Services.Content.Downloads;
using SitefinityWebApp.Services.Content.Images;
using SitefinityWebApp.Services.Content.Category;

namespace SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "TorrentWidget", Title = "Torrent Widget", SectionName = "TorrentWidgets")]

    public class TorrentWidgetController : Controller
    {


        private readonly ITorrentService _torrentService;
        private readonly ICategoryService _categoryService;
        private readonly IDownloadsService _downloadsService;
        private readonly IImagesService _imagesService;
        private readonly IMapper _mapper;

        private const string GENRES_PREFIX = "/genres";

        public TorrentWidgetController(ITorrentService torrentService, IMapper mapper, IImagesService imagesService, IDownloadsService downloadsService, ICategoryService categoryService)
        {
            _torrentService = torrentService;
            _mapper = mapper;
            _imagesService = imagesService;
            _downloadsService = downloadsService;
            _categoryService = categoryService;
        }

        public ActionResult Index()
        {
            ViewBag.Categories = _mapper.Map<List<SelectListItem>>(_categoryService.GetCategories());
            ViewBag.Images = _mapper.Map<List<SelectListItem>>(_imagesService.GetImages());
            ViewBag.DownloadUrls = _mapper.Map<List<SelectListItem>>(_downloadsService.GetDownloadUrls());

            return View("Index");
        }

        [HttpPost]
        public ActionResult Submit(TorrentWidgetModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Categories = _mapper.Map<List<SelectListItem>>(_categoryService.GetCategories());
                ViewBag.Images = _mapper.Map<List<SelectListItem>>(_imagesService.GetImages());
                ViewBag.DownloadUrls = _mapper.Map<List<SelectListItem>>(_downloadsService.GetDownloadUrls());
                return View("Index", model);
            }


            ILifecycleDataItem res = _torrentService.Create(model, SecurityManager.GetCurrentUserId());

            return Redirect($"/");


        }
    }
}