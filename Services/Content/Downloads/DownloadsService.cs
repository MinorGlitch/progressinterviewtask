﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Modules.Libraries;

namespace SitefinityWebApp.Services.Content.Downloads
{
    public class DownloadsService : IDownloadsService
    {
        private readonly LibrariesManager _libraryManager;

        public DownloadsService(LibrariesManager libraryManager)
        {
            _libraryManager = libraryManager;
        }

        public Document GetDownloadUrlById(Guid id)
        {
            return _libraryManager.GetDocument(id);
        }

        public IEnumerable<Document> GetDownloadUrls()
        {
            IEnumerable<Document> downloads = _libraryManager
                .GetDocuments()
                .Where(i => i.Status == Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Master)
                .AsEnumerable();

            return downloads;
        }

    }
}