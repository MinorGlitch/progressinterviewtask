﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Sitefinity.Libraries.Model;


namespace SitefinityWebApp.Services.Content.Downloads
{
    public interface IDownloadsService
    {
        IEnumerable<Document> GetDownloadUrls();

        Document GetDownloadUrlById(Guid id);
    }
}
