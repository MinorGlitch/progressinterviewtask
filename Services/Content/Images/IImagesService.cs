﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Sitefinity.Libraries.Model;


namespace SitefinityWebApp.Services.Content.Images
{
    public interface IImagesService
    {
        IEnumerable<Image> GetImages();
        Image GetImageById(Guid id);
    }
}
