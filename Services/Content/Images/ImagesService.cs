﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;


namespace SitefinityWebApp.Services.Content.Images
{
    public class ImagesService : IImagesService
    {
        private readonly LibrariesManager _libraryManager;

        public ImagesService(LibrariesManager libraryManager)
        {
            _libraryManager = libraryManager;
        }

        public Image GetImageById(Guid id)
        {
            return _libraryManager.GetImage(id);
        }

        public IEnumerable<Image> GetImages()
        {
            IEnumerable<Image> thumbnails = _libraryManager
                .GetImages()
                .Where(i => i.Status == Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Master)
                .AsEnumerable();

            return thumbnails;
        }
    }
}