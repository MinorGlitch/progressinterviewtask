﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Services;
using System.Text.RegularExpressions;
using Telerik.Sitefinity.Workflow;
using System.Globalization;

namespace SitefinityWebApp.Services.Content.Files
{
    public class FilesService : IFilesService
    {
        private readonly LibrariesManager _libraryManager;

        public FilesService(LibrariesManager libraryManager)
        {
            _libraryManager = libraryManager;
        }

        public Guid UploadDocument(string documentTitle, Stream documentStream, string documentFileName, string documentExtension, CultureInfo culture)
        {
            if (culture != null)
                SetCulture(culture);

            Document document = _libraryManager.CreateDocument(Guid.NewGuid());

            var documentLibraries = _libraryManager.GetDocumentLibraries().ToList();
            document.Parent = documentLibraries.FirstOrDefault(d => d.Title == "Default Library");

            document.Title = documentTitle;
            document.DateCreated = DateTime.UtcNow;
            document.PublicationDate = DateTime.UtcNow;
            document.LastModified = DateTime.UtcNow;
            document.UrlName = Regex.Replace(documentTitle.ToLower(), @"[^\w\-\!\$\'\(\)\=\@\d_]+", "-");
            document.MediaFileUrlName = Regex.Replace(documentFileName.ToLower(), @"[^\w\-\!\$\'\(\)\=\@\d_]+", "-");
            document.ApprovalWorkflowState = "Published";

            _libraryManager.Upload(document, documentStream, documentExtension);

            _libraryManager.RecompileAndValidateUrls(document);

            _libraryManager.SaveChanges();

            SetDocumentMessageWorkflow(document.Id);

            return document.Id;
        }

        public Guid UploadImage(string imageTitle, Stream imageStream, string imageFileName, string imageExtension, CultureInfo culture)
        {
            if (culture != null)
                SetCulture(culture);

            Image image = _libraryManager.CreateImage(Guid.NewGuid());

            var imageLibraries = _libraryManager.GetAlbums().ToList();

            image.Parent = imageLibraries.FirstOrDefault(a => a.Title == "Default Library");

            image.Title = imageTitle;
            image.DateCreated = DateTime.UtcNow;
            image.PublicationDate = DateTime.Now;
            image.LastModified = DateTime.Now;
            image.UrlName = Regex.Replace(imageTitle.ToLower(), @"[^\w\-\!\$\'\(\)\=\@\d_]+", "-");
            image.MediaFileUrlName = Regex.Replace(imageFileName.ToLower(), @"[^\w\-\!\$\'\(\)\=\@\d_]+", "-");

            _libraryManager.RecompileAndValidateUrls(image);

            _libraryManager.Upload(image, imageStream, imageExtension);

            _libraryManager.SaveChanges();

            //Publish the DocumentLibraries item. The live version acquires new ID.

            SetImageMessageWorkflow(image.Id);

            return image.Id;
        }

        private static void SetCulture(CultureInfo culture)
        {
            SystemManager.CurrentContext.Culture = culture;
        }

        private static void SetImageMessageWorkflow(Guid id)
        {
            var bag = new Dictionary<string, string>
            {
                { "ContentType", typeof(Image).FullName }
            };

            WorkflowManager.MessageWorkflow(id, typeof(Image), null, "Publish", false, bag);
        }

        private static void SetDocumentMessageWorkflow(Guid id)
        {

            var bag = new Dictionary<string, string>
            { { "ContentType", typeof(Document).FullName }
            };

            WorkflowManager.MessageWorkflow(id, typeof(Document), null, "Publish", false, bag);
        }
    }
}