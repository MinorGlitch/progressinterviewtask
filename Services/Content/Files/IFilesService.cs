﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SitefinityWebApp.Services.Content.Files
{
    public interface IFilesService
    {
        Guid UploadImage(string imageTitle, Stream imageStream, string imageFileName, string imageExtension, CultureInfo culture = null);

        Guid UploadDocument(string documentTitle, Stream documentStream, string documentFileName, string documentExtension, CultureInfo culture = null);
    }
}
