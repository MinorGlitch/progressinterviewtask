﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;

namespace SitefinityWebApp.Services.Content.Category
{
    public class CategoryService : ICategoryService
    {
        private readonly TaxonomyManager _taxonomyManager;

        public CategoryService(TaxonomyManager taxonomyManager)
        {
            _taxonomyManager = taxonomyManager;
        }

        public IEnumerable<HierarchicalTaxon> GetCategories()
        {
            IEnumerable<HierarchicalTaxon> categories = _taxonomyManager
                .GetTaxa<HierarchicalTaxon>()
                .Where(t => t.Taxonomy.Name == "Categories")
                .AsEnumerable();

            return categories;
        }

        public HierarchicalTaxon GetCategoryById(Guid id)
        {
            return _taxonomyManager.GetTaxon<HierarchicalTaxon>(id);
        }
    }
}