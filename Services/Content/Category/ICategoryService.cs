﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Sitefinity.Taxonomies.Model;

namespace SitefinityWebApp.Services.Content.Category
{
    public interface ICategoryService
    {
        IEnumerable<HierarchicalTaxon> GetCategories();
        HierarchicalTaxon GetCategoryById(Guid id);
    }
}
