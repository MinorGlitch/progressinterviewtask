﻿using SitefinityWebApp.Mvc.Models;
using System;
using System.Globalization;
using System.Linq;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.Data.Linq.Dynamic;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Lifecycle;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.Versioning;
using Telerik.Sitefinity.RelatedData;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Modules.Libraries;
using SitefinityWebApp.Services.Content;
using SitefinityWebApp.Factory;
using Telerik.Sitefinity.Services;
using System.IO;
using SitefinityWebApp.Services.Content.Images;
using SitefinityWebApp.Services.Content.Downloads;
using SitefinityWebApp.Services.Content.Files;

namespace SitefinityWebApp.Services.Torrent
{
    public class TorrentService : ITorrentService
    {
        private readonly DynamicModuleManagerFactory _dynamicModuleManagerFactory;
        private readonly VersionManagerFactory _versionManagerFactory;

        private readonly IDownloadsService _downloadsService;
        private readonly IImagesService _imagesService;
        private readonly IFilesService _filesService;

        public TorrentService(IFactory<VersionManager> versionManagerFactory, IFactory<DynamicModuleManager> dynamicModuleManagerFactory, IDownloadsService downloadsService, IImagesService imagesService, IFilesService filesService)
        {
            _versionManagerFactory = versionManagerFactory as VersionManagerFactory;
            _dynamicModuleManagerFactory = dynamicModuleManagerFactory as DynamicModuleManagerFactory;
            _downloadsService = downloadsService;
            _imagesService = imagesService;
            _filesService = filesService;
        }

        public ILifecycleDataItem Create(TorrentWidgetModel model, Guid userId)
        {
            Guid imageId = Guid.Empty;
            Guid torrentId = Guid.Empty;

            var locale = model.Culture.ToString();


            if (model.ImageFile != null)
            {
                imageId = _filesService.UploadImage(model.ImageFile.FileName, model.ImageFile.InputStream, 
                    model.ImageFile.FileName, Path.GetExtension(model.ImageFile.FileName), new CultureInfo(locale));
            }

            if (model.TorrentFile != null)
            {
                torrentId = _filesService.UploadDocument(model.TorrentFile.FileName, model.TorrentFile.InputStream, 
                    model.TorrentFile.FileName, Path.GetExtension(model.TorrentFile.FileName), new CultureInfo(locale));
            }


            var transactionName = string.Format("createTorrent_{0}", Guid.NewGuid());

            _dynamicModuleManagerFactory.TransactionName = transactionName;
            _versionManagerFactory.TransactionName = transactionName;

            VersionManager versionManager = _versionManagerFactory.Instantiate();

            DynamicModuleManager dynamicModuleManager = _dynamicModuleManagerFactory.Instantiate();

            SystemManager.CurrentContext.Culture = new CultureInfo(locale);

            Type torrentType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.Torrents.Torrent");
            DynamicContent torrentItem = dynamicModuleManager.CreateDataItem(torrentType);

            torrentItem.SetString("Title", model.Title, locale);
            torrentItem.SetString("Description", model.Description, locale);
            torrentItem.SetString("AdditionalInfo", model.AdditionalInfo, locale);
            torrentItem.SetValue("TorrentDateCreated", model.DateCreated);
            torrentItem.SetValue("Owner", userId);
            torrentItem.SetValue("PublicationDate", DateTime.UtcNow);

            foreach (var category in model.Genres)
            {
                torrentItem.Organizer.AddTaxa("Category", Guid.Parse(category));
            }

            var cleanUrl = Helper.SanitizeUrl(model.Title);

            torrentItem.SetString("UrlName", cleanUrl);

            var thumbnailItem = _imagesService.GetImageById(imageId == Guid.Empty ? Guid.Parse(model.ImageId) : imageId);
            if (thumbnailItem == null)
            {
                throw new InvalidOperationException("Please specify a thumbnail.");
            }

            torrentItem.CreateRelation(thumbnailItem, "Thumbnail");

            var downloadItem = _downloadsService.GetDownloadUrlById(torrentId == Guid.Empty ? Guid.Parse(model.FileId) : torrentId);
            if (downloadItem == null)
            {
                throw new InvalidOperationException("Please specify a download URL.");
            }

            torrentItem.CreateRelation(downloadItem, "Download");

            torrentItem.SetWorkflowStatus(dynamicModuleManager.Provider.ApplicationName, "Draft", new CultureInfo(locale));

            versionManager.CreateVersion(torrentItem, false);

            ILifecycleDataItem publishedTorrentItem = dynamicModuleManager.Lifecycle.Publish(torrentItem);

            torrentItem.SetWorkflowStatus(dynamicModuleManager.Provider.ApplicationName, "Published", new CultureInfo(locale));

            versionManager.CreateVersion(torrentItem, true);

            TransactionManager.CommitTransaction(transactionName);

            return publishedTorrentItem;
        }
    }
}