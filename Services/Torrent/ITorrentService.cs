﻿using SitefinityWebApp.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Lifecycle;

namespace SitefinityWebApp.Services.Torrent
{
    public interface ITorrentService
    {
        ILifecycleDataItem Create(TorrentWidgetModel model, Guid userId);
    }
}