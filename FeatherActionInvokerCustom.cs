﻿using System;
using System.Threading;
using Telerik.Microsoft.Practices.Unity;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Frontend.Mvc.Infrastructure.Routing;
using Telerik.Sitefinity.Mvc;

namespace SitefinityWebApp
{
    public class FeatherActionInvokerCustom : FeatherActionInvoker
    {
        protected override void HandleControllerException(System.Exception err)
        {
            // Propagate Exceptions to Application Level
            if (!(err is ThreadAbortException))
                throw new Exception(err.Message, err);
        }

        internal static void Register()
        {
            ObjectFactory.Container.RegisterType<IControllerActionInvoker, FeatherActionInvokerCustom>();
        }
    }
}