﻿using AutoMapper;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Sitefinity.Taxonomies.Model;

namespace SitefinityWebApp
{
    public class AutomapperModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IMapper>().ToMethod(AutoMapper).InSingletonScope();
        }

        private IMapper AutoMapper(Ninject.Activation.IContext context)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<HierarchicalTaxon, SelectListItem>()
                    .ForMember(sli => sli.Text, opt => opt.MapFrom(ht => ht.FullUrl))
                    .ForMember(sli => sli.Value, opt => opt.MapFrom(ht => ht.Id.ToString()))
                    .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<Telerik.Sitefinity.Libraries.Model.Image, SelectListItem>()
                   .ForMember(sli => sli.Text, opt => opt.MapFrom(i => i.MediaFileUrlName))
                   .ForMember(sli => sli.Value, opt => opt.MapFrom(i => i.Id.ToString()))
                   .ForAllOtherMembers(opt => opt.Ignore());

                cfg.CreateMap<Telerik.Sitefinity.Libraries.Model.Document, SelectListItem>()
                  .ForMember(sli => sli.Text, opt => opt.MapFrom(i => i.MediaFileUrlName))
                  .ForMember(sli => sli.Value, opt => opt.MapFrom(i => i.Id.ToString()))
                  .ForAllOtherMembers(opt => opt.Ignore());
            });

            var mapper = config.CreateMapper();

            return mapper;
        }
    }
}