﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.Versioning;

namespace SitefinityWebApp.Factory
{
    public class VersionManagerFactory : IFactory<VersionManager>
    {
        public string TransactionName { get; set; }
        public string Provider { get { return null; } }

        public VersionManager Instantiate()
        {
            return VersionManager.GetManager(Provider, TransactionName);
        }
    }
}