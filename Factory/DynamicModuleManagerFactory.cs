﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.DynamicModules;

namespace SitefinityWebApp.Factory
{
    public class DynamicModuleManagerFactory : IFactory<DynamicModuleManager>
    {
        public string TransactionName { get; set; }
        public string Provider { get { return string.Empty; } }

        public DynamicModuleManager Instantiate()
        {
            return DynamicModuleManager.GetManager(Provider, TransactionName);
        }
    }
}