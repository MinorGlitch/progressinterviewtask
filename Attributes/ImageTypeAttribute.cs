﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace SitefinityWebApp.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class ImageTypeAttribute : ValidationAttribute
    {

        private List<string> AllowedExtensions { get; set; }

        public ImageTypeAttribute()
        {
            AllowedExtensions = ".jpg,.jpeg,.png".Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public override bool IsValid(object value)
        {
            HttpPostedFileBase file = value as HttpPostedFileBase;

            if (file != null)
            {
                var fileName = file.FileName;

                return AllowedExtensions.Contains(Path.GetExtension(fileName));
            }

            return true;
        }
    }
}