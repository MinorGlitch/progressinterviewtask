﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace SitefinityWebApp
{
    public static class Helper
    {
        public static string SanitizeUrl(string title)
        {

            return Regex.Replace(title, "[{|}|\\|\\^|~|\\[|\\]|:\\;`]", "").Replace(' ', '-').ToLower();
        }
    }
}