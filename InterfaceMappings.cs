﻿using Ninject.Modules;
using SitefinityWebApp.Factory;
using SitefinityWebApp.Services.Content;
using SitefinityWebApp.Services.Content.Category;
using SitefinityWebApp.Services.Content.Downloads;
using SitefinityWebApp.Services.Content.Files;
using SitefinityWebApp.Services.Content.Images;
using SitefinityWebApp.Services.Torrent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Versioning;

namespace SitefinityWebApp
{
    public class InterfaceMappings : NinjectModule
    {
        public override void Load()
        {
            this.Bind<ITorrentService>().To<TorrentService>();

            var taxonomyManager = TaxonomyManager.GetManager();
            var librariesManager = LibrariesManager.GetManager();

            this.Bind<TaxonomyManager>().ToMethod(context => taxonomyManager).InSingletonScope();
            this.Bind<LibrariesManager>().ToMethod(context => librariesManager).InSingletonScope();

            this.Bind<IFactory<DynamicModuleManager>>().To<DynamicModuleManagerFactory>();
            this.Bind<IFactory<VersionManager>>().To<VersionManagerFactory>();
            this.Bind<ICategoryService>().To<CategoryService>();
            this.Bind<IImagesService>().To<ImagesService>();
            this.Bind<IDownloadsService>().To<DownloadsService>();
            this.Bind<IFilesService>().To<FilesService>();
        }
    }
}